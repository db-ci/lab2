let inicio = 2;
let fim = 20;
for (let i = inicio; i <= fim; i++){
    if (i % 2 == 0 && i > 0) {
        console.log(i)
    }
}

let j = inicio;
while (j <= fim){
    if (j % 2 == 0 && j > 0) {
        console.log(j);
    }
    j++;
}

function min(x: number, y: number): number {
    if (x < y)
        return x;
    else
        return y;
}

let menor = min(8, 6);
console.log(menor);

function pow_iterativa(x: number, y: number) {
    let mult: number = 1;
    if (y == 0)
        return 1;
    if (x > 0 && y > 0) {
        for (let i = 0; i < y; i ++){
            mult = mult * x;
        }
    return mult;
    }
}

function pow_recursiva(x: number, y: number) {
    let mult: number = 1;
    if (y == 0)
        return 1;
    if (x > 0 && y > 0) {
        mult = x * pow_recursiva(x, y - 1);
        }
    return mult;
}

console.log(pow_recursiva(3, 0))
console.log(pow_recursiva(4, 2))

console.log(pow_iterativa(3, 0))
console.log(pow_iterativa(4, 2))

function toMaiusculaPrimeira(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

console.log(toMaiusculaPrimeira('cintia'));

function getMax(arr: number[]): number {
    let maior = arr[0];
    for (let i = 1; i <= arr.length; i++){
        if (arr[i] > maior)
            maior = arr[i];
    }
    return maior;
}

let resultado = getMax([6, 8, 2, 1, 9, 4]);
console.log(resultado);

function frequencia(vetor: number[]): Map<number,number> {
    let conta = new Map<number,number>();
    for (let valor of vetor) {
        if (conta.has(valor)) {
            conta.set(valor, conta.get(valor)!+1);
        } else {
            conta.set(valor,1);
        }
    }
    return conta;
}

console.log(frequencia([5,5,3,7,4,1,0,0,0,9]));
console.log(frequencia([]));

function frequenciaV2(vetor: number[]): Map<number,number> {
    return vetor.reduce((conta, valor) => 
    conta.set(valor, (conta.get(valor)||0)+1), 
    new Map<number,number>());
}

console.log(frequenciaV2([5,5,3,7,4,1,0,0,0,9]));